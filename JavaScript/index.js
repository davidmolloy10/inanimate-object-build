//Code has been developed mainly with the help of gomakethings.com to get the correct functionality working changes have been made to facilitate this exact use case, original code can be found here https://gomakethings.com/detecting-select-menu-changes-with-vanilla-js/ 

//Listen for an input
document.addEventListener('input', function (event) {

	// Only look for the form with an ID of scent
	if (event.target.id !== 'scent') return;

    /**
     * If the value selected is summerBerry it will apply an according background gradient to the product section on the website (same applys for the other values below)
     */
	if (event.target.value === "summerBerry") {
		document.getElementById("product").style.backgroundImage =
        "linear-gradient(41deg, rgba(91,0,114,1) 0%, rgba(203,0,0,1) 100%)";
	}

    if (event.target.value === "pinkGelateo") {
		document.getElementById("product").style.backgroundImage  = "linear-gradient(34deg, rgba(184,19,118,1) 32%, rgba(255,145,215,1) 100%)";

		/**
		 * Added code to change the images and their alt tags within the target event on the select form
		 */
		document.getElementById("productImg").src="Assets/Pink.jpg";
		document.getElementById("productImg").alt="Our Pink Gelateo Candle Product in the 12oz jar";
		document.getElementById("burnImg").src="Assets/PinkBurn.jpg";
		document.getElementById("burnImg").alt="Image of our Pink Gelateo candle burning";
	}

    if (event.target.value === "saltedPopcorn") {
		document.getElementById("product").style.backgroundImage  = "linear-gradient(38deg, rgba(255,200,0,1) 0%, rgba(255,138,0,1) 58%)";

		document.getElementById("productImg").src="Assets/Popcorn.jpg";
		document.getElementById("productImg").alt="Our Salted Caramel Popcorn Candle Product in the 12oz jar";
		document.getElementById("burnImg").src="Assets/PopcornBurn.jpg";
		document.getElementById("burnImg").alt="Image of our Salted Caramel Popcorn candle burning";
	}    

}, false);